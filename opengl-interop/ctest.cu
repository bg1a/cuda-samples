#include <stdio.h>
#include <stdlib.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include <GLES3/gl31.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glext.h>

#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>

extern void create_window(int width, int height);
extern void window_loop(void);

static const int win_width = 1920;
static const int win_height = 1200;

static const int img_width = 1920;
static const int img_height = 1200;
static const int img_bytes_per_pixel = 4;
static const int img_size = img_width * img_height * img_bytes_per_pixel;
const char *bin_file = "./test_img_bgrx.raw";

// Cuda, image source at Cuda device
unsigned char *d_src = NULL;
// Cuda, image destination at Cuda device
unsigned char *d_dst = NULL;
// OpenGL resource registered to Cuda
struct cudaGraphicsResource *cu_rsrc;


// OpenGL, constant data and global variables

GLfloat vertices[] = {
    //Vertex        Texture
    -1.0, -1.0,     0.0, 1.0,
    -1.0,  1.0,     0.0, 0.0,
     1.0, -1.0,     1.0, 1.0,
     1.0,  1.0,     1.0, 0.0,
};

GLuint program;
GLuint vao, vbo, cbo, tex;

static const char *v_shader = 
"#version 330 core \n"
"layout (location=0) in vec2 vPos; \n"
"layout (location=1) in vec2 tPos; \n"
"out vec2 texcoord; \n"
"void main(void) \n"
"{ \n"
"  gl_Position = vec4(vPos, 0.0, 1.0); \n"
"  texcoord = tPos; \n"
"} \n";

static const char *frag_shader =
"#version 330 core \n"
"in vec2 texcoord; \n"
"uniform sampler2D tex; \n"
"out vec4 FragColor; \n"
"void main(void) \n"
"{ \n"
"  FragColor = texture(tex, texcoord); \n"
"} \n";

#define checkCudaErrors(err) __checkCudaErrors(err, __LINE__)
void __checkCudaErrors(cudaError_t err, int linenum)
{
    if (cudaSuccess != err){
        fprintf(stderr, "line %d, checkCudaErrors: %s\n", linenum, cudaGetErrorString(err));
    }
}

__global__ 
void img_bgrx_to_rgba(unsigned char *dst, unsigned char *src, int width, int height, int bytes_per_pixel)
{
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;

    if((row < height) && (col < width)){
        int pix_off = row*width*bytes_per_pixel + col*bytes_per_pixel;

        dst[pix_off]     = src[pix_off + 2];
        dst[pix_off + 1] = src[pix_off + 1];
        dst[pix_off + 2] = src[pix_off];
        dst[pix_off + 3] = src[pix_off + 3];
    }
}

void process_img_cuda(void)
{
    dim3 threads_per_block(32, 32);
    dim3 blocks( (img_width + threads_per_block.x - 1) / threads_per_block.x, 
                 (img_height + threads_per_block.y - 1) / threads_per_block.y );

    checkCudaErrors(cudaMallocManaged((void**)&d_dst, img_size, cudaMemAttachGlobal));
    if(d_dst && d_src){
        img_bgrx_to_rgba<<<blocks, threads_per_block>>>(d_dst, d_src, img_width, img_height, img_bytes_per_pixel);
    }
}

void upload_img_to_cuda(void)
{
    int f = open(bin_file, O_RDONLY);
    struct stat buf = {0};

    if((f > 0) && !fstat(f, &buf) && (buf.st_size == img_size)){
        off_t fsize = buf.st_size;
        void *data = NULL;

        data = mmap(NULL, fsize, PROT_READ, MAP_SHARED, f, 0);
        if(data){
            checkCudaErrors(cudaMallocManaged((void**)&d_src, fsize, cudaMemAttachGlobal));

            cudaMemcpy(d_src, data, fsize, cudaMemcpyHostToDevice); 

            munmap(data, fsize);
        }else{
            d_src = NULL;
        }
    }else{
        printf("Failed to read file!\n");
        printf("Make sure that file: %s exists and it contains RGBA data of size %d\n", bin_file, img_size);
        d_src = NULL;
    }
    close(f);
}

void cleanup(void)
{
    if(d_src){
        cudaFree(d_src);
    }
    if(d_dst){
        cudaFree(d_dst);
    }
}

void draw_opengl()
{
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH);
    glDisable(GL_LIGHTING);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

int main(int argc, char **argv)
{
#if defined(__linux__)
    setenv("DISPLAY", ":0", 0);
#endif
    create_window(win_width, win_height);

    GLuint vs = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vs, 1, (const GLchar **) &v_shader, NULL);
    glCompileShader(vs);
    
    GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fs, 1, (const GLchar **) &frag_shader, NULL);
    glCompileShader(fs);
    
    program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);
    glUseProgram(program);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4*sizeof(float), (void*)(2*sizeof(float)));
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img_width, img_height, 
            0, GL_RGBA, GL_UNSIGNED_BYTE, NULL); 


    upload_img_to_cuda();
    process_img_cuda();

    checkCudaErrors( cudaGraphicsGLRegisterImage(&cu_rsrc, tex, GL_TEXTURE_2D, cudaGraphicsRegisterFlagsWriteDiscard)); 

    checkCudaErrors( cudaGraphicsMapResources(1, &cu_rsrc, 0));

    cudaArray *cu_rsrc_array;
    checkCudaErrors( cudaGraphicsSubResourceGetMappedArray(&cu_rsrc_array, cu_rsrc, 0, 0));

    checkCudaErrors( cudaMemcpyToArray(cu_rsrc_array, 0, 0, d_dst, img_size, cudaMemcpyDeviceToDevice));

    checkCudaErrors( cudaGraphicsUnmapResources(1, &cu_rsrc, 0));

    window_loop();

    cleanup();
    return 0;
}

