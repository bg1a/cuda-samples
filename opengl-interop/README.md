EGL-Cuda Interoperation Test
============================

Overview
--------
This application creates an EGL window and fils it with the image obtained from Cuda.

Reference
---------
For EGL window, see *NVIDIA_CUDA-9.0_Samples/.../graphics_interface.c*

Cuda to OpenGL mapping, see *NVIDIA_CUDA-9.0_Samples/3_Imaging/simpleCUDA2GL*

Build
-----
Run Makefile

Run
---
```
./ctest
```

Links
-----
[Cuda-OpenGL Interops](https://www.nvidia.com/content/gtc/documents/1055_gtc09.pdf)

[Cuda Programming Guide](https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html)

[GLES-OpenGL differences](https://pandorawiki.org/Porting_to_GLES_from_GL)

[OpenGL Tutorial] (http://www.opengl-tutorial.org)

[Cuda-OpenGL Interop, Demo] (https://github.com/nvpro-samples/gl_cuda_interop_pingpong_st)
