#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <cuda.h>
#include <cuda_runtime.h>


char *loadPtxSource(const char *ptx_source_path)
{
    FILE *fs = fopen(ptx_source_path, "r");
    char *src = NULL;

    if(fs){
        long length, size;
        
        fseek(fs, 0, SEEK_END);
        length = ftell(fs);
        size = length + 1;

        src = (char *)calloc(size, sizeof(char));
        if(src){
            rewind(fs);
            fread(src, size, length, fs);
        }else{
            printf("Failed to allocate buffer!\n");
        }

        fclose(fs);
    }else{
        printf("Failed to open the file: %s\n", ptx_source_path);
    }

    return src;
}


int main(int argc, char **argv)
{
    const unsigned int nThreads = 256;
    const unsigned int nBlocks = 64;
    const size_t memSize = nThreads * nBlocks * sizeof(int);
    char *ptx_src = loadPtxSource("test.ptx");

    CUcontext ctx;
    int cuda_device = 0;
    cudaDeviceProp deviceProp;

    CUmodule hModule = 0;
    CUfunction hKernel = 0;
    CUlinkState lState;
    CUjit_option options[1];
    void *optionVals[1];
    float walltime;
    void *cuOut;
    size_t outSize;
    int myErr = 0;

    options[0] = CU_JIT_WALL_TIME;
    optionVals[0] = (void*)&walltime;

    cudaSetDevice(cuda_device);
    cudaGetDeviceProperties(&deviceProp, cuda_device);
    printf("Cuda device: %s\n", deviceProp.name);
    printf(" compute capability: %d.%d\n", deviceProp.major, deviceProp.minor);

    cuDevicePrimaryCtxRetain(&ctx, cuda_device);

    myErr = cuLinkCreate(1, options, optionVals, &lState);
    if(CUDA_SUCCESS == myErr){
        myErr = cuLinkAddData(lState, CU_JIT_INPUT_PTX, (void*)ptx_src, strlen(ptx_src) + 1, 0, 0, 0, 0);
        if(myErr == CUDA_SUCCESS){
            cuLinkComplete(lState, &cuOut, &outSize);

            myErr = cuModuleLoadData(&hModule, cuOut);
            if(myErr) printf("Failed to load data!\n");

            myErr = cuModuleGetFunction(&hKernel, hModule, "ptx_kernel");
            if(myErr) printf("Failed to get function!\n");

            cuLinkDestroy(lState);
        }else{
            printf("Failed to link data!\n");
        }
    }else{
        printf("Failed to create a link (%d)!\n", myErr);
    }

    cuFuncSetBlockShape(hKernel, nThreads, 1, 1);

    int *d_data = NULL;
    int *h_data = NULL;
    cudaMalloc(&d_data, memSize);
    h_data = (int*)calloc(memSize, sizeof(int));

    int paramOffset = 0;
    cuParamSetv(hKernel, paramOffset, &d_data, sizeof(d_data));
    paramOffset += sizeof(d_data);
    cuParamSetSize(hKernel, paramOffset);
    myErr = cuLaunchGrid(hKernel, nBlocks, 1);
    if(myErr) printf("Failed to launch grid!\n");

    cuDevicePrimaryCtxRelease(cuda_device);

    cudaMemcpy(h_data, d_data, memSize, cudaMemcpyDeviceToHost);
    printf("Data output:\n");
    for(int j = 0; j < nThreads * nBlocks; ++j){
        printf("%d ", h_data[j]);
        if(j && ((j + 1) % 16 == 0)) printf("\n");
    }
    printf("\n");

    free(h_data);
    cudaFree(d_data);
    cudaDeviceReset();

    return 0;
}







