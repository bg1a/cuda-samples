Load PTX Source into Cuda
=========================

Overview
--------
In this example a PTX source is loaded into Nvidia driver and executed on GPU

Build and Run
-------------
- run *make*
- run *make run*

